<?php 

return [
    1 => [
        "id" => 1,
        "name" => "Default template",
        "group" => "Custom",
        "module" => "Kitt3nPimcoreCustomBundle",
        "controller" => "@KITT3N\\Pimcore\\CustomBundle\\Controller\\DefaultController",
        "action" => "index",
        "template" => NULL,
        "type" => "page",
        "priority" => 0,
        "creationDate" => 1563545262,
        "modificationDate" => 1563545407,
        "legacy" => FALSE
    ]
];
