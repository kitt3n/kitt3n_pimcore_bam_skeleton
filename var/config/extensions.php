<?php 

return [
    "bundle" => [
        "KITT3N\\Pimcore\\ElementsBundle\\Kitt3nPimcoreElementsBundle" => TRUE,
        "KITT3N\\Pimcore\\LayoutsBundle\\Kitt3nPimcoreLayoutsBundle" => TRUE,
        "KITT3N\\Pimcore\\BulmaBundle\\Kitt3nPimcoreBulmaBundle" => FALSE,
        "KITT3N\\Pimcore\\CustomBundle\\Kitt3nPimcoreCustomBundle" => TRUE,
        "KITT3N\\Pimcore\\RestrictionsBundle\\Kitt3nPimcoreRestrictionsBundle" => TRUE
    ]
];
