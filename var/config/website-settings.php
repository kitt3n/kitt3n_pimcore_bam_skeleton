<?php 

return [
    4 => [
        "id" => 4,
        "name" => "bam.root.document",
        "language" => "de",
        "type" => "document",
        "data" => 2,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564062231
    ],
    5 => [
        "id" => 5,
        "name" => "bam.root.tag",
        "language" => "",
        "type" => "text",
        "data" => "1",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1562768854
    ],
    6 => [
        "id" => 6,
        "name" => "bam.custom.logo",
        "language" => "",
        "type" => "asset",
        "data" => 4,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564062457
    ],
    7 => [
        "id" => 7,
        "name" => "bam.custom.visual",
        "language" => NULL,
        "type" => "asset",
        "data" => "",
        "siteId" => NULL,
        "creationDate" => NULL,
        "modificationDate" => 1563348297
    ],
    8 => [
        "id" => 8,
        "name" => "bam.restrictions.group.guest",
        "language" => "",
        "type" => "object",
        "data" => 4,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564062248
    ],
    9 => [
        "id" => 9,
        "name" => "bam.restrictions.login.redirect",
        "language" => "de",
        "type" => "document",
        "data" => 2,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564062257
    ],
    10 => [
        "id" => 10,
        "name" => "bam.footer.imprint",
        "language" => "de",
        "type" => "document",
        "data" => 6,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564064849
    ],
    11 => [
        "id" => 11,
        "name" => "bam.footer.privacy",
        "language" => NULL,
        "type" => "document",
        "data" => "",
        "siteId" => NULL,
        "creationDate" => NULL,
        "modificationDate" => 1563543081
    ],
    12 => [
        "id" => 12,
        "name" => "bam.footer.contact",
        "language" => NULL,
        "type" => "document",
        "data" => "",
        "siteId" => NULL,
        "creationDate" => NULL,
        "modificationDate" => 1563543092
    ],
    13 => [
        "id" => 13,
        "name" => "bam.root.document",
        "language" => "en",
        "type" => "document",
        "data" => NULL,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564062224
    ],
    14 => [
        "id" => 14,
        "name" => "bam.restrictions.login.redirect",
        "language" => "en",
        "type" => "document",
        "data" => NULL,
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564062226
    ],
    15 => [
        "id" => 15,
        "name" => "bam.custom.brand",
        "language" => "",
        "type" => "text",
        "data" => "Customer<br>Xxx",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1564407382
    ]
];
